#pragma once

#include <le/gl/types.hpp>

namespace le::gl
{
  enum texture_format
  {
    #define FORMAT(f) f,

    #include <le/gl/texture_format.def>
  };

  gl_enum value_of(texture_format format);

  gl_enum base_of(texture_format format);
}