#pragma once

#include <le/gl/ishader.hpp>

namespace le::gl
{
  struct iprogram
  {
    virtual ~iprogram(void) { }

    virtual iprogram& attach_shader(const ishader& shader) = 0;

    virtual const iprogram& link(void) const = 0;

    virtual gl_uint name(void) const = 0;

    virtual gl_int attribute_location(const std::string& name) const = 0;

    virtual gl_int uniform_location(const std::string& name) const = 0;

    virtual iprogram& uniform(gl_int location, const mat4& value) = 0;
    
    virtual iprogram& uniform(const std::string& name, const mat4& value) = 0;
  };
}