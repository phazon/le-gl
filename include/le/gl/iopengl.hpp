#pragma once

#include <string>

#include <le/gl/types.hpp>

namespace le::gl
{
  struct iprogram;
  struct ivertex_array;
  struct itexture;

  struct iopengl
  {
    virtual void initialize(
      std::function<void (*(const char*))()> resolver,
      bool lazy = false
    ) const = 0;

    virtual void viewport(
      gl_int x,
      gl_int y,
      gl_sizei width,
      gl_sizei height
    ) const = 0;

    virtual gl_uint create_shader(gl_enum type) const = 0;

    virtual void shader_source(
      gl_uint shader,
      const std::string& source
    ) const = 0;

    virtual void compile_shader(gl_uint shader) const = 0;

    virtual void delete_shader(gl_uint shader) const = 0;

    virtual gl_uint create_program(void) const = 0;

    virtual void use_program(gl_uint program) = 0;

    virtual void use_program(const iprogram& program) = 0;

    virtual const gl_uint& program(void) const = 0;

    virtual void delete_program(gl_uint program) const = 0;

    virtual void attach_shader(gl_uint program, gl_uint shader) const = 0;

    virtual void link_program(gl_uint program) const = 0;

    virtual gl_int get_attribute_location(
      gl_uint program,
      const std::string& name
    ) const = 0;

    virtual gl_int get_uniform_location(
      gl_uint program,
      const std::string& name
    ) const = 0;

    virtual void program_uniform_matrix4fv(
      gl_uint program,
      gl_int location,
      bool transpose,
      const gl_float *value
    ) const = 0;

    virtual gl_uint create_vertex_array(void) const = 0;

    virtual void vertex_array_attribute_format(
      gl_uint vao,
      gl_uint attribute_index,
      gl_int size,
      gl_enum type,
      bool normalzed,
      gl_uint relative_offset
    ) const = 0;

    virtual void enable_vertex_array_attribute(
      gl_uint vao,
      gl_uint attribute_index
    ) const = 0;

    virtual void vertex_array_vertex_buffer(
      gl_uint vao,
      gl_uint binding_index,
      gl_uint buffer,
      gl_intptr offset,
      gl_size stride
    ) const = 0;

    virtual void vertex_array_element_buffer(
      gl_uint vao,
      gl_uint buffer
    ) const = 0;

    virtual void bind_vertex_array(gl_uint vao) = 0;

    virtual void bind_vertex_array(const ivertex_array& vao) = 0;

    virtual const gl_uint& vertex_array(void) const = 0;

    virtual void delete_vertex_array(gl_uint vao) const = 0;

    virtual gl_uint create_buffer(void) const = 0;

    virtual void delete_buffer(gl_uint buffer) const = 0;

    virtual void named_buffer_data(
      const gl_uint buffer,
      const gl_size size,
      const void *data,
      const gl_enum usage
    ) const = 0;

    virtual void clear_color(const color& color) = 0;

    virtual const color& clear_color(void) const = 0;

    virtual void clear(clear_buffer_mask mask) const = 0;

    virtual void get_float_v(gl_enum name, gl_float *data) const = 0;

    virtual void draw_elements(
      gl_enum mode,
      gl_size count,
      gl_enum type,
      const void *offset
    ) const = 0;

    virtual void enable(gl_enum capability) = 0;

    virtual void disable(gl_enum capability) = 0;

    virtual void depth_func(gl_enum func) = 0;

    virtual gl_uint create_texture(gl_enum target) const = 0;

    virtual void delete_texture(gl_uint texture) const = 0;

    virtual void texture_storage_2d(
      gl_uint texture,
      gl_sizei levels,
      gl_enum internal_format,
      gl_sizei width,
      gl_sizei height
    ) const = 0;

    virtual void texture_sub_image_2d(
      gl_uint texture,
      gl_int level,
      gl_int x_offset,
      gl_int y_offset,
      gl_sizei width,
      gl_sizei height,
      gl_enum format,
      gl_enum type,
      const void *pixels
    ) const = 0;

    virtual void texture_sub_image_3d(
      gl_uint texture,
      gl_int level,
      gl_int x_offset,
      gl_int y_offset,
      gl_int z_offset,
      gl_sizei width,
      gl_sizei height,
      gl_sizei depth,
      gl_enum format,
      gl_enum type,
      const void *pixels
    ) const = 0;

    virtual void bind_texture_unit(gl_uint unit, gl_uint texture) const = 0;

    virtual void bind_texture_unit(
      gl_uint unit, 
      const itexture& texture
    ) const = 0;

    virtual gl_uint create_framebuffer(void) const = 0;

    virtual void delete_framebuffer(gl_uint framebuffer) const = 0;

    virtual void named_framebuffer_texture(
      gl_uint framebuffer,
      gl_enum attachment,
      gl_uint texture,
      gl_int level
    ) const = 0;

    virtual void named_framebuffer_draw_buffers(
      gl_uint framebuffer,
      const std::vector<gl_enum> buffers
    ) const = 0;
  };
}