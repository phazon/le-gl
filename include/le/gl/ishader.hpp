#pragma once

#include <string>

#include <le/gl/types.hpp>

namespace le::gl
{
  struct ishader
  {
    virtual ~ishader(void) { }

    virtual ishader& set_source(const std::string& source) = 0;

    virtual const ishader& compile(void) const = 0;

    virtual gl_uint name(void) const = 0;
  };
}