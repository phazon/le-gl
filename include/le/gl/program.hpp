#pragma once

#include <memory>

#include <le/gl/iprogram.hpp>
#include <le/gl/iopengl.hpp>
#include <le/gl/shader_type.hpp>

#include <boost/di/extension/injector.hpp>

namespace le::gl
{
  class program: public iprogram
  {

  public:

    explicit program(iopengl& gl);

    program& attach_shader(const ishader& shader) override;

    const program& link(void) const override;

    gl_uint name(void) const override;

    gl_int attribute_location(const std::string& attribute) const override;

    gl_int uniform_location(const std::string& name) const override;

    program& uniform(gl_int location, const mat4& value) override;

    program& uniform(const std::string& name, const mat4& value) override;

  private:

    iopengl& gl;

    std::shared_ptr<gl_uint> id;

    std::shared_ptr<gl_uint> create(void) const;
  };
}