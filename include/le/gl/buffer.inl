namespace le::gl
{
  template<typename T>
  buffer<T>::buffer(iopengl& gl) :
    gl(gl)
  {
    id = create();
  }

  template<typename T>
  gl_uint buffer<T>::name(void) const
  {
    return *id;
  }

  template<typename T>
  gl_uint buffer<T>::value_size(void) const
  {
    return sizeof(value_type);
  }

  template<typename T>
  gl_uint buffer<T>::length(void) const
  {
    return bytes / value_size();
  }

  template<typename T>
  gl_enum buffer<T>::type(void) const
  {
    return enum_for<T>();
  }

  template<typename T>
  std::shared_ptr<gl_uint> buffer<T>::create(void) const
  {
    return std::shared_ptr<gl_uint>(
      new gl_uint(gl.create_buffer()),
      [&gl = gl](auto *ptr)
      {
        gl.delete_buffer(*ptr);

        delete ptr;
      }
    );
  }

  template<typename T>
  buffer<T>& buffer<T>::set_data(
    const gl_size size,
    const void *data,
    const buffer_usage usage)
  {
    gl.named_buffer_data(
      name(),
      size,
      data,
      value_of(usage)
    );

    bytes = size;

    return *this;
  }

  template<typename T>
  buffer<T>& buffer<T>::set_data(
    const std::vector<T>& data,
    const buffer_usage usage)
  {
    return set_data(size(data), data.data(), usage);
  }

  template<typename T>
  gl_size size(const std::vector<T>& data)
  {      
    const auto type = sizeof(T);
    const auto size = data.size();

    return static_cast<gl_size>(size * type);
  }
}