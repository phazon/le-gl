#pragma once

#include <le/gl/types.hpp>

namespace le::gl
{
  enum framebuffer_attachment
  {
    #define ATTACHMENT(a) a,

    #include <le/gl/framebuffer_attachment.def>
  };

  gl_enum value_of(framebuffer_attachment attachment);
}