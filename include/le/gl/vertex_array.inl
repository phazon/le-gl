#include <le/gl/offset_of.hpp>

namespace le::gl
{
  template<typename T>
  vertex_array& vertex_array::with_attribute_format(
    gl_uint attribute_index,
    gl_int size,
    gl_uint relative_offset,
    bool normalized)
  {
    gl.vertex_array_attribute_format(
      name(),
      attribute_index,
      size,
      enum_for<T>(),
      normalized,
      relative_offset
    );

    return *this;
  }

  template<typename V>
  vertex_array& vertex_array::with_vector_attribute(
    gl_uint attribute_index,
    gl_uint relative_offset,
    bool normalized)
  {
    return with_attribute_format<typename V::value_type>(
      attribute_index,
      V::length(),
      relative_offset,
      normalized
    );
  }

  template<typename V>
  vertex_array& vertex_array::with_vector_attribute(
    gl_uint attribute_index,
    const ibuffer& buffer,
    gl_uint relative_offset,
    bool normalized)
  {
    return
      with_attribute_format<typename V::value_type>(
        attribute_index,
        V::length(),
        relative_offset,
        normalized
      )
      .vertex_buffer(attribute_index, buffer, relative_offset)
      .enable_attribute(attribute_index);;
  }

  template<typename T, typename M>
  vertex_array& vertex_array::with_vector_attribute(
    gl_uint attribute_index,
    M T::*member,
    bool normalized)
  {
    return with_vector_attribute<M>(
      attribute_index,
      offset_of(member),
      normalized
    );
  }

  template<typename T, typename M>
  vertex_array& vertex_array::with_vector_attribute(
    gl_uint attribute_index,
    M T::*member,
    const buffer<T>& buffer,
    bool normalized)
  {
    return with_vector_attribute(
      attribute_index,
      member,
      static_cast<const ibuffer&>(buffer),
      normalized
    );
  }

  template<typename T, typename M>
  vertex_array& vertex_array::with_vector_attribute(
    gl_uint attribute_index,
    M T::*member,
    const ibuffer& buffer,
    bool normalized)
  {
    return with_vector_attribute(attribute_index, member, normalized)
      .vertex_buffer(attribute_index, buffer)
      .enable_attribute(attribute_index);
  }
}