#pragma once

#include <memory>

#include <le/gl/buffer.hpp>
#include <le/gl/ivertex_array.hpp>
#include <le/gl/iopengl.hpp>
#include <le/gl/enum.hpp>

namespace le::gl
{
  class vertex_array: public ivertex_array
  {

  public:

    vertex_array(iopengl& gl);

    gl_uint name(void) const override;

    template<typename T>
    vertex_array& with_attribute_format(
      gl_uint attribute_index,
      gl_int size,
      gl_uint relative_offset,
      bool normalized = false
    );

    template<typename V>
    vertex_array& with_vector_attribute(
      gl_uint attribute_index,
      gl_uint relative_offset = 0,
      bool normalized = false
    );

    template<typename V>
    vertex_array& with_vector_attribute(
      gl_uint attribute_index,
      const ibuffer& buffer,
      gl_uint relative_offset = 0,
      bool normalized = false
    );

    template<typename T, typename M>
    vertex_array& with_vector_attribute(
      gl_uint attribute_index,
      M T::*member,
      bool normalized = false
    );

    template<typename T, typename M>
    vertex_array& with_vector_attribute(
      gl_uint attribute_index,
      M T::*member,
      const buffer<T>& buffer,
      bool normalized = false
    );

    template<typename T, typename M>
    vertex_array& with_vector_attribute(
      gl_uint attribute_index,
      M T::*member,
      const ibuffer& buffer,
      bool normalized = false
    );

    vertex_array& enable_attribute(gl_uint index) override;

    vertex_array& vertex_buffer(
      gl_uint index,
      const ibuffer& buffer,
      gl_intptr offset = 0
    ) override;

    vertex_array& element_buffer(const ibuffer& buffer) override;

    void draw_elements(gl_enum mode = GL_TRIANGLES) const override;

  private:

    iopengl& gl;

    std::shared_ptr<gl_uint> id;

    std::shared_ptr<gl_uint> create(void) const;

    const ibuffer *ebo;

  };
}

#include <le/gl/vertex_array.inl>