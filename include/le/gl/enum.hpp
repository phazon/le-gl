#pragma once

#include <glbinding/gl45core/enum.h>
#include <le/gl/types.hpp>

namespace le::gl
{  
  using namespace ::gl45core;

  template<typename T>
  gl_enum enum_for(void)
  {
    return GL_NONE;
  };
}

#include <le/gl/enum.inl>