#pragma once

#include <le/gl/types.hpp>

#include <glm/ext/matrix_transform.hpp>

namespace le::gl
{
  template<typename T, typename glm::qualifier Q>
  auto look_at(
    const vec<3, T, Q>& eye,
    const vec<3, T, Q>& center,
    const vec<3, T, Q>& up
  ) -> decltype(glm::lookAt(eye, center, up))
  {
    return glm::lookAt(eye, center, up);
  }

  using glm::perspective;
  using glm::rotate;
  using glm::radians;
}