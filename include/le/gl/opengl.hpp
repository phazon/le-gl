#pragma once

#include <le/gl/iopengl.hpp>

namespace le::gl
{
  class opengl: public iopengl
  {

  public:

    void initialize(
      std::function<void (*(const char*))()> resolver,
      bool lazy
    ) const override;

    void viewport(
      gl_int x,
      gl_int y,
      gl_sizei width,
      gl_sizei height
    ) const override;

    gl_uint create_shader(gl_enum type) const override;

    void shader_source(
      gl_uint shader,
      const std::string& source
    ) const override;

    void compile_shader(gl_uint shader) const override;

    void delete_shader(gl_uint shader) const override;

    gl_uint create_program(void) const override;

    void use_program(gl_uint program) override;

    void use_program(const iprogram& program) override;

    const gl_uint& program(void) const override;

    void delete_program(gl_uint program) const override;

    void attach_shader(gl_uint program, gl_uint shader) const override;

    void link_program(gl_uint program) const override;

    gl_int get_attribute_location(
      gl_uint program,
      const std::string& name
    ) const;

    gl_int get_uniform_location(
      gl_uint program,
      const std::string& name
    ) const;

    void program_uniform_matrix4fv(
      gl_uint program,
      gl_int location,
      bool transpose,
      const gl_float *value
    ) const;

    gl_uint create_vertex_array(void) const override;

    void vertex_array_attribute_format(
      gl_uint vao,
      gl_uint attribute_index,
      gl_int size,
      gl_enum type,
      bool normalzed,
      gl_uint relative_offset
    ) const override;

    void enable_vertex_array_attribute(
      gl_uint vao,
      gl_uint attribute_index
    ) const override;

    void vertex_array_vertex_buffer(
      gl_uint vao,
      gl_uint binding_index,
      gl_uint buffer,
      gl_intptr offset,
      gl_size stride
    ) const override;

    void vertex_array_element_buffer(
      gl_uint vao,
      gl_uint buffer
    ) const override;

    void bind_vertex_array(gl_uint vao) override;

    void bind_vertex_array(const ivertex_array& vao) override;

    const gl_uint& vertex_array(void) const override;

    void delete_vertex_array(gl_uint vao) const override;

    gl_uint create_buffer(void) const override;

    void delete_buffer(gl_uint buffer) const override;

    void named_buffer_data(
      const gl_uint buffer,
      const gl_size size,
      const void *data,
      const gl_enum usage
    ) const override;

    void clear_color(const color& color) override;

    const color& clear_color(void) const;

    void clear(clear_buffer_mask mask) const override;

    void get_float_v(gl_enum name, gl_float *data) const override;

    void draw_elements(
      gl_enum mode,
      gl_size count,
      gl_enum type,
      const void *offset
    ) const override;

    void enable(gl_enum capability) override;

    void disable(gl_enum capability) override;

    void depth_func(gl_enum func) override;

    gl_uint create_texture(gl_enum target) const override;

    void delete_texture(gl_uint texture) const override;

    void texture_storage_2d(
      gl_uint texture,
      gl_sizei levels,
      gl_enum internal_format,
      gl_sizei width,
      gl_sizei height
    ) const override;

    void texture_sub_image_2d(
      gl_uint texture,
      gl_int level,
      gl_int x_offset,
      gl_int y_offset,
      gl_sizei width,
      gl_sizei height,
      gl_enum format,
      gl_enum type,
      const void *pixels
    ) const override;

    void texture_sub_image_3d(
      gl_uint texture,
      gl_int level,
      gl_int x_offset,
      gl_int y_offset,
      gl_int z_offset,
      gl_sizei width,
      gl_sizei height,
      gl_sizei depth,
      gl_enum format,
      gl_enum type,
      const void *pixels
    ) const override;

    void bind_texture_unit(gl_uint unit, gl_uint texture) const override;

    void bind_texture_unit(
      gl_uint unit, 
      const itexture& texture
    ) const override;

    gl_uint create_framebuffer(void) const override;

    void delete_framebuffer(gl_uint name) const override;

    void named_framebuffer_texture(
      gl_uint framebuffer,
      gl_enum attachment,
      gl_uint texture,
      gl_int level
    ) const override;

    void named_framebuffer_draw_buffers(
      gl_uint framebuffer,
      const std::vector<gl_enum> buffers
    ) const override;

  private:
    
    gl_uint program_;

    gl_uint vao_;

    color clear_color_;

  };
}