#pragma once

#include <le/gl/opengl.hpp>

#include <boost/di.hpp>

namespace le::gl
{
  auto module(void)
  {
    return boost::di::make_injector(
      boost::di::bind<le::gl::iopengl>.to<le::gl::opengl>()
    );
  }
}