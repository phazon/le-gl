#pragma once

#include <memory>
#include <vector>

#include <le/gl/ibuffer.hpp>
#include <le/gl/iopengl.hpp>

namespace le::gl
{
  template<typename T>
  class buffer: public ibuffer
  {

  public:

    typedef T value_type;

    explicit buffer(iopengl& gl);

    gl_uint name(void) const override;

    gl_uint value_size(void) const override;

    gl_uint length(void) const override;

    gl_enum type(void) const override;

    buffer<T>& set_data(
      const gl_size size,
      const void *data,
      const buffer_usage usage
    ) override;

    buffer<T>& set_data(
      const std::vector<T>& data,
      const buffer_usage usage
    );

  private:

    iopengl& gl;

    std::shared_ptr<gl_uint> id;

    gl_size bytes;

    std::shared_ptr<gl_uint> create(void) const;

  };

  template<typename T>
  gl_size size(const std::vector<T>& data);
}

#include <le/gl/buffer.inl>