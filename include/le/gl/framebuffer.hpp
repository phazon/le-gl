#pragma once

#include <memory>

#include <le/gl/iframebuffer.hpp>
#include <le/gl/iopengl.hpp>

namespace le::gl
{
  class framebuffer: public iframebuffer
  {
  public:

    explicit framebuffer(iopengl& gl);

    gl_uint name(void) const override;

    framebuffer& targets(
      const std::map<
        framebuffer_attachment,
        std::reference_wrapper<const itexture>
      >& targets
    ) override;

  private:

    iopengl& gl;

    std::shared_ptr<gl_uint> id;

    std::shared_ptr<gl_uint> create(void) const;
  };
}