namespace le::gl
{
  template<texture_target T, texture_format F, int L>
  texture<T, F, L>::texture(iopengl& gl) :
    gl(gl)
  {
    id = create();
  }

  template<texture_target T, texture_format F, int L>
  gl_uint texture<T, F, L>::name(void) const
  {
    return *id;
  }

  
  template<texture_target T, texture_format F, int L>
  texture<T, F, L>& texture<T, F, L>::allocate_storage(gl_sizei width, gl_sizei height)
  {
    if (allocated) { return *this; }

    allocate(gl, *this, width, height);
    allocated = true;

    return *this;
  }

  template<texture_target T, texture_format F, int L>
  texture<T, F, L>& texture<T, F, L>::upload(
    gl_sizei width,
    gl_sizei height,
    const std::vector<gl_ubyte>& pixels)
  {
    allocate_storage(width, height);
    update(gl, *this, width, height, pixels);

    return *this;
  }

  template<texture_target T, texture_format F, int L>
  texture<T, F, L>& texture<T, F, L>::upload(
    gl_sizei width,
    gl_sizei height,
    const std::array<std::vector<gl_ubyte>, 6>& cubemap)
  {
    allocate_storage(width, height);
    update(gl, *this, width, height, cubemap);

    return *this;
  }

  template<texture_target T, texture_format F, int L>
  template<typename V>
  texture<T, F, L>& texture<T, F, L>::upload(const V& value)
  {
    texture_uploader<V>::upload(*this, value);

    return *this;
  }

  template<texture_target T, texture_format F, int L>
  std::shared_ptr<gl_uint> texture<T, F, L>::create(void) const
  {
    return std::shared_ptr<gl_uint>(
      new gl_uint(gl.create_texture(value_of(T))),
      [&gl = gl](auto *ptr)
      {
        gl.delete_texture(*ptr);

        delete ptr;
      }
    );
  }

  template<texture_target T, texture_format F, int L, typename V>
  void upload_adl(texture<T, F, L>& texture, const V& value)
  {
    upload(texture, value);
  }

  template<typename V>
  template<texture_target T, texture_format F, int L>
  void texture_uploader<V>::upload(texture<T, F, L>& texture, const V& value)
  {
    upload_adl(texture, value);
  }

  template<texture_format F, int L>
  void allocate(
    iopengl& gl,
    const texture<tex2d, F, L>& texture,
    gl_sizei width,
    gl_sizei height)
  {
    gl.texture_storage_2d(
      texture.name(),
      texture.levels,
      value_of(texture.format),
      width,
      height
    );
  }

  template<texture_format F, int L>
  void allocate(
    iopengl& gl,
    const texture<cubemap, F, L>& texture,
    gl_sizei width,
    gl_sizei height)
  {
    gl.texture_storage_2d(
      texture.name(),
      texture.levels,
      value_of(texture.format),
      width,
      height
    );
  }

  template<texture_format F, int L>
  void update(
    iopengl& gl,
    const texture<tex2d, F, L>& texture,
    gl_sizei width,
    gl_sizei height,
    const std::vector<gl_ubyte>& pixels)
  {
    gl.texture_sub_image_2d(
      texture.name(),
      0,
      0,
      0,
      width,
      height,
      base_of(texture.format),
      enum_for<gl_ubyte>(),
      static_cast<const void *>(pixels.data())
    );
  }

  template<texture_format F, int L>
  void update(
    iopengl& gl,
    const texture<cubemap, F, L>& texture,
    gl_sizei width,
    gl_sizei height,
    const std::array<std::vector<gl_ubyte>, 6>& cubemap)
  {
    for (std::size_t i = 0; i < cubemap.size(); i++)
    {
      const auto& pixels = cubemap.at(i);

      gl.texture_sub_image_3d(
        texture.name(),
        0,
        0,
        0,
        i,
        width,
        height,
        1,
        base_of(texture.format),
        enum_for<gl_ubyte>(),
        static_cast<const void *>(pixels.data())
      );
    }
  }
}