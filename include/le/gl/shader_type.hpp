#pragma once

#include <le/gl/types.hpp>

namespace le::gl
{
  enum shader_type
  {
    #define SHADER(s) s,

    #include <le/gl/shader.def>
  };

  gl_enum value_of(shader_type type);
}