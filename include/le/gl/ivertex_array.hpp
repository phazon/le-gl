#pragma once

#include <le/gl/types.hpp>
#include <le/gl/ibuffer.hpp>

namespace le::gl
{
  struct ivertex_array
  {
    virtual ~ivertex_array(void) { }

    virtual gl_uint name(void) const = 0;

    virtual ivertex_array& enable_attribute(gl_uint index) = 0;

    virtual ivertex_array& vertex_buffer(
      gl_uint index,
      const ibuffer& buffer,
      gl_intptr offset = 0
    ) = 0;

    virtual ivertex_array& element_buffer(const ibuffer& buffer) = 0;

    virtual void draw_elements(gl_enum mode) const = 0;
  };
}