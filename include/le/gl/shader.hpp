#pragma once

#include <memory>

#include <le/gl/ishader.hpp>
#include <le/gl/iopengl.hpp>
#include <le/gl/shader_type.hpp>

namespace le::gl
{
  template<shader_type T>
  class shader: public ishader
  {

  public:

    explicit shader(iopengl& gl);

    shader<T>& set_source(const std::string& source) override;

    const shader<T>& compile(void) const override;

    gl_uint name(void) const override;

  private:

    iopengl& gl;

    std::shared_ptr<gl_uint> id;

    std::shared_ptr<gl_uint> create(void) const;

  };
}

#include <le/gl/shader.inl>