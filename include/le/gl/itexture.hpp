#pragma once

#include <array>
#include <vector>

#include <le/gl/types.hpp>

namespace le::gl
{
  struct itexture
  {
    virtual ~itexture(void) { }
    virtual gl_uint name(void) const = 0;
    virtual itexture& allocate_storage(gl_sizei width, gl_sizei height) = 0;
    virtual itexture& upload(
      gl_sizei width,
      gl_sizei height,
      const std::vector<gl_ubyte>& pixels
    ) = 0;
    virtual itexture& upload(
      gl_sizei width,
      gl_sizei height,
      const std::array<std::vector<gl_ubyte>, 6>& pixels
    ) = 0;
  };
}