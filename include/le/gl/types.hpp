#pragma once

#include <glbinding/gl45core/types.h>
#include <glm/glm.hpp>
#include <glm/gtc/type_ptr.hpp>

namespace le::gl
{
  using gl_enum = gl45core::GLenum;
  using gl_byte = gl45core::GLbyte;
  using gl_ubyte = gl45core::GLubyte;
  using gl_int = gl45core::GLint;
  using gl_uint = gl45core::GLuint;
  using gl_float = gl45core::GLfloat;
  using gl_size = gl45core::GLsizeiptr;
  using gl_sizei = gl45core::GLsizei;
  using gl_intptr = gl45core::GLintptr;
  using gl_boolean = gl45core::GLboolean;

  using ::gl::GL_FALSE;
  using ::gl::GL_TRUE;

  using clear_buffer_mask = gl45core::ClearBufferMask;

  using glm::value_ptr;

  using glm::vec;
  using glm::vec2;
  using glm::vec3;
  using glm::vec4;

  using glm::ivec2;

  using glm::mat3;
  using glm::mat4;

  using color = glm::vec4;
}