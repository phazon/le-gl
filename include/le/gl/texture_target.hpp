#pragma once

#include <le/gl/types.hpp>

namespace le::gl
{
  enum texture_target
  {
    #define TARGET(t) t,

    #include <le/gl/texture.def>
  };
  
  gl_enum value_of(texture_target target);
}