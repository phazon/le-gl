#pragma once

#include <memory>

#include <le/gl/itexture.hpp>
#include <le/gl/iopengl.hpp>
#include <le/gl/enum.hpp>
#include <le/gl/texture_target.hpp>
#include <le/gl/texture_format.hpp>

namespace le::gl
{
  template<texture_target Target, texture_format Format = RGBA8, int Levels = 1>
  class texture: public itexture
  {
  public:

    explicit texture(iopengl& gl);

    gl_uint name(void) const override;

    texture<Target, Format, Levels>& allocate_storage(
      gl_sizei width,
      gl_sizei height
    ) override;

    texture<Target, Format, Levels>& upload(
      gl_sizei width,
      gl_sizei height,
      const std::vector<gl_ubyte>& pixels
    ) override;

    texture<Target, Format, Levels>& upload(
      gl_sizei width,
      gl_sizei height,
      const std::array<std::vector<gl_ubyte>, 6>& cubemap
    ) override;

    template<typename Value>
    texture<Target, Format, Levels>& upload(const Value& value);

    static constexpr texture_target target = Target;
    static constexpr texture_format format = Format;
    static constexpr int levels = Levels;

  private:

    iopengl& gl;

    std::shared_ptr<gl_uint> id;

    bool allocated = false;

    std::shared_ptr<gl_uint> create(void) const;
  };

  template<texture_format Format = RGBA8, int Levels = 1>
  using texture2d = texture<tex2d, Format, Levels>;

  template<texture_format Format = RGBA8, int Levels = 1>
  using textureCube = texture<cubemap, Format, Levels>;

  template<typename Value>
  struct texture_uploader
  {
    template<texture_target Target, texture_format Format, int Levels>
    static void upload(
      texture<Target, Format, Levels>& texture,
      const Value& value
    );
  };

  template<
    texture_target Target,
    texture_format Format,
    int Levels,
    typename Value
  >
  void upload(texture<Target, Format, Levels>& texture, const Value& value);

  template<texture_target T, texture_format F, int L>
  void allocate(
    iopengl& gl,
    const texture<T, F, L>& texture,
    gl_sizei width,
    gl_sizei height
  );

  template<texture_target T, texture_format F, int L>
  void update(
    iopengl& gl,
    const texture<T, F, L>& texture,
    gl_sizei width,
    gl_sizei height,
    const std::vector<gl_ubyte>& pixels
  ) {}

  template<texture_target T, texture_format F, int L>
  void update(
    iopengl& gl,
    const texture<T, F, L>& texture,
    gl_sizei width,
    gl_sizei height,
    const std::array<std::vector<gl_ubyte>, 6>& cubemap
  ) {}
}

#include <le/gl/texture.inl>