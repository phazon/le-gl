namespace le::gl
{
  #define ENUM_FOR_TYPE(type, enum) \
    template<> gl_enum enum_for<type>(void);

  #include <le/gl/enum_for_type.def>
}