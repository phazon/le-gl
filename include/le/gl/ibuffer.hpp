#pragma once

#include <le/gl/types.hpp>
#include <le/gl/buffer_usage.hpp>
#include <le/gl/enum.hpp>

namespace le::gl
{
  struct ibuffer
  {
    virtual ~ibuffer(void) { }

    virtual gl_uint name(void) const = 0;

    virtual gl_uint value_size(void) const = 0;
    
    virtual gl_uint length(void) const = 0;

    virtual gl_enum type(void) const = 0;

    virtual ibuffer& set_data(
      const gl_size size,
      const void *data,
      const buffer_usage usage
    ) = 0;
  };
}