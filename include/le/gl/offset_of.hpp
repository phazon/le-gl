#pragma once

namespace le::gl
{
  template <typename T, typename M>
  inline size_t constexpr offset_of(M T::*member)
  {
    constexpr T object{};
    
    return size_t(&(object.*member)) - size_t(&object);
  }
}