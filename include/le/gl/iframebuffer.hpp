#pragma once

#include <map>
#include <functional>

#include <le/gl/types.hpp>
#include <le/gl/itexture.hpp>
#include <le/gl/framebuffer_attachment.hpp>

namespace le::gl
{
  struct iframebuffer
  {
    virtual ~iframebuffer(void) { }

    virtual gl_uint name(void) const = 0;

    virtual iframebuffer& targets(
      const std::map<
        framebuffer_attachment,
        std::reference_wrapper<const itexture>
      >& targets
    ) = 0;
  };
}