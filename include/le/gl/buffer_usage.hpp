#pragma once

#include <le/gl/types.hpp>

namespace le::gl
{
  enum buffer_usage
  {
    #define USAGE(u) u
    #include <le/gl/usage.def>
  };

  gl_enum value_of(const buffer_usage usage);
}