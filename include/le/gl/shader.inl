namespace le::gl
{
  template<shader_type T>
  shader<T>::shader(iopengl& gl) :
    gl(gl)
  {
    id = create();
  }

  template<shader_type T>
  shader<T>& shader<T>::set_source(const std::string& source)
  {
    gl.shader_source(name(), source);

    return *this;
  }

  template<shader_type T>
  const shader<T>& shader<T>::compile(void) const
  {
    gl.compile_shader(name());

    return *this;
  }

  template<shader_type T>
  gl_uint shader<T>::name(void) const
  {
    return *id;
  }

  template<shader_type T>
  std::shared_ptr<gl_uint> shader<T>::create(void) const
  {
    return std::shared_ptr<gl_uint>(
      new gl_uint(gl.create_shader(value_of(T))),
      [&gl = gl](auto *ptr)
      {
        gl.delete_shader(*ptr);

        delete ptr;
      }
    );
  }
}