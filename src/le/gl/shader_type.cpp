#include <le/gl/shader_type.hpp>
#include <le/gl/enum.hpp>

namespace le::gl
{
  gl_enum value_of(shader_type type)
  {
    constexpr gl_enum values[] = {
      GL_VERTEX_SHADER,
      GL_GEOMETRY_SHADER,
      GL_FRAGMENT_SHADER,
    };

    return values[static_cast<unsigned int>(type)];
  }
}