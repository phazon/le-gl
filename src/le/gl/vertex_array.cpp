#include <le/gl/vertex_array.hpp>

namespace le::gl
{
  vertex_array::vertex_array(iopengl& gl) :
    gl(gl)
  {
    id = create();
  }

  gl_uint vertex_array::name(void) const
  {
    return *id;
  }

  std::shared_ptr<gl_uint> vertex_array::create(void) const
  {
    return std::shared_ptr<gl_uint>(
      new gl_uint(gl.create_vertex_array()),
      [&gl = gl](auto *ptr)
      {
        gl.delete_vertex_array(*ptr);

        delete ptr;
      }
    );
  }

  vertex_array& vertex_array::enable_attribute(gl_uint index)
  {
    gl.enable_vertex_array_attribute(name(), index);

    return *this;
  }

  vertex_array& vertex_array::vertex_buffer(
    gl_uint index,
    const ibuffer& buffer,
    gl_intptr offset)
  {
    gl.vertex_array_vertex_buffer(
      name(),
      index,
      buffer.name(),
      offset,
      buffer.value_size()
    );

    return *this;
  }

  vertex_array& vertex_array::element_buffer(const ibuffer& buffer)
  {
    gl.vertex_array_element_buffer(name(), buffer.name());

    ebo = &buffer;

    return *this;
  }

  void vertex_array::draw_elements(gl_enum mode) const
  {
    if (!ebo) { return; }

    gl.draw_elements(mode, ebo->length(), ebo->type(), 0);
  }
}