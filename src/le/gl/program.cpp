#include <le/gl/program.hpp>

namespace le::gl
{
  program::program(iopengl& gl) :
    gl(gl)
  {
    id = create();
  }

  program& program::attach_shader(const ishader& shader)
  {
    gl.attach_shader(name(), shader.name());

    return *this;
  }

  const program& program::link(void) const
  {
    gl.link_program(name());

    return *this;
  }

  gl_uint program::name(void) const
  {
    return *id;
  }

  gl_int program::attribute_location(const std::string& attribute) const
  {
    return gl.get_attribute_location(name(), attribute);
  }

  gl_int program::uniform_location(const std::string& attribute) const
  {
    return gl.get_uniform_location(name(), attribute);
  }

  program& program::uniform(gl_int location, const mat4& value)
  {
    gl.program_uniform_matrix4fv(name(), location, false, value_ptr(value));

    return *this;
  }

  program& program::uniform(const std::string& name, const mat4& value)
  {
    return uniform(uniform_location(name), value);
  }

  std::shared_ptr<gl_uint> program::create(void) const
  {
    return std::shared_ptr<gl_uint>(
      new gl_uint(gl.create_program()),
      [&gl = gl](auto *ptr) {
        gl.delete_program(*ptr);

        delete ptr;
      }
    );
  }
}