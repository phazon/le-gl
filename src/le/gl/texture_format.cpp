#include <le/gl/texture_format.hpp>
#include <le/gl/enum.hpp>

#include <map>

namespace le::gl
{
  gl_enum value_of(texture_format format)
  {
    constexpr const gl_enum values[] = {
      GL_RGBA8,
    };

    return values[static_cast<unsigned int>(format)];
  }

  gl_enum base_of(texture_format format)
  {
    const std::map<texture_format, gl_enum> map = {
      {RGBA8, GL_RGBA}
    };

    return map.at(format);
  }
}