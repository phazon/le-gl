#include <le/gl/texture_target.hpp>
#include <le/gl/enum.hpp>

namespace le::gl
{
  gl_enum value_of(texture_target target)
  {
    constexpr gl_enum values[] = {
      GL_TEXTURE_2D,
      GL_TEXTURE_CUBE_MAP
    };

    return values[static_cast<unsigned int>(target)];
  }
}