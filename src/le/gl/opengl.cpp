#include <le/gl/opengl.hpp>
#include <le/gl/functions.hpp>
#include <le/gl/enum.hpp>
#include <le/gl/iprogram.hpp>
#include <le/gl/ivertex_array.hpp>
#include <le/gl/itexture.hpp>

#include <glbinding/glbinding.h>

namespace le::gl
{
  void opengl::initialize(
    std::function<void (*(const char*))()> resolver,
    bool lazy) const
  {
    glbinding::initialize(resolver, !lazy);
  }

  void opengl::viewport(
    gl_int x,
    gl_int y,
    gl_sizei width,
    gl_sizei height) const
  {
    glViewport(x, y, width, height); 
  }

  gl_uint opengl::create_shader(gl_enum type) const
  {
    return glCreateShader(type);
  }

  void opengl::shader_source(gl_uint shader, const std::string& source) const
  {
    const char *c_str = source.c_str();

    glShaderSource(shader, 1, &c_str, nullptr);
  }

  void opengl::compile_shader(gl_uint shader) const
  {
    glCompileShader(shader);
  }

  void opengl::delete_shader(gl_uint shader) const
  {
    glDeleteShader(shader);
  }

  gl_uint opengl::create_program(void) const
  {
    return glCreateProgram();
  }

  void opengl::use_program(gl_uint program)
  {
    glUseProgram(program);
    
    program_ = program;
  }

  void opengl::use_program(const iprogram& program)
  {
    use_program(program.name());
  }

  const gl_uint& opengl::program(void) const
  {
    return program_;
  }

  void opengl::delete_program(gl_uint program) const
  {
    glDeleteProgram(program);
  }

  void opengl::attach_shader(gl_uint program, gl_uint shader) const
  {
    glAttachShader(program, shader);
  }

  void opengl::link_program(gl_uint program) const
  {
    glLinkProgram(program);
  }

  gl_int opengl::get_attribute_location(
    gl_uint program,
    const std::string& name) const
  {
    return glGetAttribLocation(program, name.c_str());
  }

  gl_int opengl::get_uniform_location(
    gl_uint program,
    const std::string& name) const
  {
    return glGetUniformLocation(program, name.c_str());
  }

  void opengl::program_uniform_matrix4fv(
    gl_uint program,
    gl_int location,
    bool transpose,
    const gl_float *value) const
  {
    const gl_sizei count = 1;

    glProgramUniformMatrix4fv(
      program,
      location,
      count,
      transpose ? GL_TRUE : GL_FALSE,
      value
    );
  }

  gl_uint opengl::create_vertex_array(void) const
  {
    gl_uint vao;
    glCreateVertexArrays(1, &vao);

    return vao;
  }

  void opengl::vertex_array_attribute_format(
    gl_uint vao,
    gl_uint attribute_index,
    gl_int size,
    gl_enum type,
    bool normalzed,
    gl_uint relative_offset) const
  {
    glVertexArrayAttribFormat(
      vao,
      attribute_index,
      size,
      type,
      normalzed,
      relative_offset
    );
  }

  void opengl::enable_vertex_array_attribute(
    gl_uint vao,
    gl_uint attribute_index) const
  {
    glEnableVertexArrayAttrib(vao, attribute_index);
  }

  void opengl::vertex_array_vertex_buffer(
    gl_uint vao,
    gl_uint binding_index,
    gl_uint buffer,
    gl_intptr offset,
    gl_size stride) const
  {
    glVertexArrayVertexBuffer(vao, binding_index, buffer, offset, stride);
  }

  void opengl::vertex_array_element_buffer(gl_uint vao, gl_uint buffer) const
  {
    glVertexArrayElementBuffer(vao, buffer);
  }

  void opengl::bind_vertex_array(gl_uint vao)
  {
    glBindVertexArray(vao);

    vao_ = vao;
  }

  void opengl::bind_vertex_array(const ivertex_array& vao)
  {
    bind_vertex_array(vao.name());
  }

  const gl_uint& opengl::vertex_array(void) const
  {
    return vao_;
  }

  void opengl::delete_vertex_array(gl_uint vao) const
  {
    glDeleteVertexArrays(1, &vao);
  }

  gl_uint opengl::create_buffer(void) const
  {
    gl_uint buffer;
    glCreateBuffers(1, &buffer);

    return buffer;
  }

  void opengl::delete_buffer(gl_uint buffer) const
  {
    glDeleteBuffers(1, &buffer);
  }

  void opengl::named_buffer_data(
    const gl_uint buffer,
    const gl_size size,
    const void *data,
    const gl_enum usage) const
  {
    glNamedBufferData(buffer, size, data, usage);
  }

  void opengl::clear_color(const color& colour)
  {
    glClearColor(colour.r, colour.g, colour.b, colour.a);

    clear_color_ = colour;
  }

  const color& opengl::clear_color(void) const
  {
    return clear_color_;
  }

  void opengl::clear(clear_buffer_mask mask) const
  {
    glClear(mask);
  }

  void opengl::get_float_v(gl_enum name, gl_float *data) const
  {
    glGetFloatv(name, data);
  }

  void opengl::draw_elements(
    gl_enum mode,
    gl_size count,
    gl_enum type,
    const void *offset) const
  {
    glDrawElements(mode, count, type, offset); 
  }

  void opengl::enable(gl_enum capability)
  {
    glEnable(capability);
  }

  void opengl::disable(gl_enum capability)
  {
    glDisable(capability);
  }

  void opengl::depth_func(gl_enum func)
  {
    glDepthFunc(func);
  }

  gl_uint opengl::create_texture(gl_enum target) const
  {
    gl_uint texture;
    glCreateTextures(target, 1, &texture);

    return texture;
  }

  void opengl::delete_texture(gl_uint texture) const
  {
    glDeleteTextures(1, &texture);
  }

  void opengl::texture_storage_2d(
    gl_uint texture,
    gl_sizei levels,
    gl_enum internal_format,
    gl_sizei width,
    gl_sizei height) const
  {
    glTextureStorage2D(texture, levels, internal_format, width, height);
  }

  void opengl::texture_sub_image_2d(
    gl_uint texture,
    gl_int level,
    gl_int x_offset,
    gl_int y_offset,
    gl_sizei width,
    gl_sizei height,
    gl_enum format,
    gl_enum type,
    const void *pixels) const
  {
    glTextureSubImage2D(
      texture,
      level,
      x_offset,
      y_offset,
      width,
      height,
      format,
      type,
      pixels
    );
  }

  void opengl::texture_sub_image_3d(
    gl_uint texture,
    gl_int level,
    gl_int x_offset,
    gl_int y_offset,
    gl_int z_offset,
    gl_sizei width,
    gl_sizei height,
    gl_sizei depth,
    gl_enum format,
    gl_enum type,
    const void *pixels) const
  {
    glTextureSubImage3D(
      texture,
      level,
      x_offset,
      y_offset,
      z_offset,
      width,
      height,
      depth,
      format,
      type,
      pixels
    );
  }

  void opengl::bind_texture_unit(gl_uint unit, gl_uint texture) const
  {
    glBindTextureUnit(unit, texture);
  }

  void opengl::bind_texture_unit(gl_uint unit, const itexture& texture) const
  {
    bind_texture_unit(unit, texture.name());
  }

  gl_uint opengl::create_framebuffer(void) const
  {
    gl_uint framebuffer;

    glCreateFramebuffers(1, &framebuffer);

    return framebuffer;
  }

  void opengl::delete_framebuffer(gl_uint framebuffer) const
  {
    glDeleteFramebuffers(1, &framebuffer);
  }

  void opengl::named_framebuffer_texture(
    gl_uint framebuffer,
    gl_enum attachment,
    gl_uint texture,
    gl_int level
  ) const {
    glNamedFramebufferTexture(framebuffer, attachment, texture, level);
  }

  void opengl::named_framebuffer_draw_buffers(
    gl_uint framebuffer,
    const std::vector<gl_enum> buffers
  ) const {
    glNamedFramebufferDrawBuffers(framebuffer, buffers.size(), buffers.data());
  }

  gl_int shader_iv(
    gl_uint shader,
    gl_enum parameter)
  {
    GLint value;

    glGetShaderiv(shader, parameter, &value);

    return value;
  }

  std::string shader_info_log(gl_uint shader)
  {
    auto length = shader_iv(shader, GL_INFO_LOG_LENGTH);

    std::vector<char> log(length);

    glGetShaderInfoLog(shader, length, &length, log.data());

    return std::string(log.data(), length);
  }

  gl_int program_iv(
    const gl_uint program,
    const gl_enum parameter)
  {
    gl_int value;

    glGetProgramiv(program, parameter, &value);

    return value;
  }

  std::string program_info_log(const gl_uint program)
  {
    auto length = program_iv(program, GL_INFO_LOG_LENGTH);

    std::vector<char> log(length);

    glGetProgramInfoLog(program, length, &length, log.data());

    return std::string(log.data(), length);
  }
}