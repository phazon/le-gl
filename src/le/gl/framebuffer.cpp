#include <le/gl/framebuffer.hpp>

#include <type_traits>

namespace
{
  template<typename Iterable, typename F>
  auto map(const Iterable& iterable, F f)
  {
    std::vector<std::invoke_result_t<F, typename Iterable::value_type>> result;

    std::transform(
      iterable.begin(),
      iterable.end(),
      std::back_inserter(result),
      f
    );

    return result;
  }
}

namespace le::gl
{
  framebuffer::framebuffer(iopengl& gl) : gl(gl)
  {
    id = create();
  }

  gl_uint framebuffer::name(void) const
  {
    return *id;
  }

  framebuffer& framebuffer::targets(
    const std::map<
      framebuffer_attachment,
      std::reference_wrapper<const itexture>
    >& targets)
  {
    for (const auto& [attachment, texture] : targets)
    {
      gl.named_framebuffer_texture(
        name(),
        value_of(attachment),
        texture.get().name(),
        0
      );
    }

    const auto buffers = map(targets, [](const auto& kvp)
    {
      const auto& [key, _] = kvp;

      return value_of(key);
    });

    gl.named_framebuffer_draw_buffers(name(), buffers);
    
    return *this;
  }

  std::shared_ptr<gl_uint> framebuffer::create(void) const
  {
    return std::shared_ptr<gl_uint>(
      new gl_uint(gl.create_framebuffer()),
      [&gl = gl](auto *ptr) {
        gl.delete_framebuffer(*ptr);

        delete ptr;
      }
    );
  }
}

