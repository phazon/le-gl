#include <le/gl/buffer_usage.hpp>
#include <le/gl/enum.hpp>

namespace le::gl
{
  gl_enum value_of(const buffer_usage usage)
  {
    // Must be in the same order as Usage enum values
    constexpr gl_enum values[] = {
      GL_STREAM_DRAW,
      GL_STREAM_READ,
      GL_STREAM_COPY,
      GL_STATIC_DRAW,
      GL_STATIC_READ,
      GL_STATIC_COPY,
      GL_DYNAMIC_DRAW,
      GL_DYNAMIC_READ,
      GL_DYNAMIC_COPY
    };

    return values[static_cast<unsigned int>(usage)];
  }
}