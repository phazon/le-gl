#include <le/gl/framebuffer_attachment.hpp>
#include <le/gl/enum.hpp>

namespace le::gl
{
  gl_enum value_of(framebuffer_attachment attachment)
  {
    constexpr gl_enum values[] = {
      GL_COLOR_ATTACHMENT0,
      GL_COLOR_ATTACHMENT1,
      GL_COLOR_ATTACHMENT2,
      GL_COLOR_ATTACHMENT3,
      GL_COLOR_ATTACHMENT4,
      GL_COLOR_ATTACHMENT5,
      GL_COLOR_ATTACHMENT6,
      GL_COLOR_ATTACHMENT7,
    };

    return values[static_cast<unsigned int>(attachment)];
  }
}
