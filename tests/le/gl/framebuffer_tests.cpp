#include <catch2/catch.hpp>
#include <fakeit/fakeit.hpp>

#include <boost/di.hpp>

#include <le/gl/iopengl.hpp>
#include <le/gl/framebuffer.hpp>

using namespace boost::di;
using namespace fakeit;
using namespace le;
using namespace le::gl;

TEST_CASE("framebuffer", "[le][le::gl][le::gl::framebuffer]")
{
  const gl_uint name = 123;

  Mock<iopengl> gl;

  Fake(
    Method(gl, delete_framebuffer),
    Method(gl, named_framebuffer_texture),
    Method(gl, named_framebuffer_draw_buffers)
  );

  When(Method(gl, create_framebuffer)).AlwaysReturn(name);

  const auto injector = make_injector(
    bind<iopengl>.to(gl.get())
  );

  auto framebuffer = injector.create<le::gl::framebuffer>();

  SECTION("creates framebuffer")
  {
    Verify(Method(gl, create_framebuffer));
  }

  SECTION("destructor")
  {
    static_cast<iframebuffer&>(framebuffer)
      .~iframebuffer();

    SECTION("deletes buffer")
    {
      Verify(Method(gl, delete_framebuffer).Using(name));
    }
  }

  SECTION("returns name")
  {
    REQUIRE(framebuffer.name() == name);
  }

  SECTION("targets")
  {
    const gl_uint texture_name1 = 21;
    const gl_uint texture_name2 = 22;

    Mock<itexture> texture1, texture2;
    When(Method(texture1, name)).AlwaysReturn(texture_name1);
    When(Method(texture2, name)).AlwaysReturn(texture_name2);

    const std::map<
      framebuffer_attachment,
      std::reference_wrapper<const itexture>
    > targets = {
      {color0, texture1.get()},
      {color1, texture2.get()}
    };

    const auto& result = framebuffer.targets(targets);

    SECTION("returns self")
    {
      REQUIRE(&result == &framebuffer);
    }

    SECTION("attaches textures to target attachments")
    {
      Verify(
        Method(gl, named_framebuffer_texture).Using(
          name,
          value_of(color0),
          texture_name1,
          0
        )
      );

      Verify(
        Method(gl, named_framebuffer_texture).Using(
          name,
          value_of(color1),
          texture_name2,
          0
        )
      );
    }

    SECTION("sets draw duffers")
    {
      Verify(
        Method(gl, named_framebuffer_draw_buffers).Matching(
          [&](gl_uint framebuffer, const std::vector<gl_enum>& buffers)
          {
            return framebuffer == name
              && buffers[0] == value_of(color0)
              && buffers[1] == value_of(color1);
          }
        )
      );
    }
  }
}