#include <catch2/catch.hpp>
#include <fakeit/fakeit.hpp>

#include <boost/di.hpp>

#include <le/gl/texture.hpp>
#include <le/gl/iopengl.hpp>
#include <le/gl/enum.hpp>

using namespace boost::di;
using namespace fakeit;
using namespace le;
using namespace le::gl;

struct image
{
  const gl_sizei width;
  const gl_sizei height;
  const std::vector<gl_ubyte> pixels;
};

template<texture_target T, texture_format F, int L>
void upload(texture<T, F, L>& texture, const image& image)
{
  texture.upload(image.width, image.height, image.pixels);
}

TEST_CASE("texture<tex2d>", "[le][le::gl][le::gl::texture]")
{
  const gl_uint name = 123;

  Mock<iopengl> gl;

  Fake(
    Method(gl, delete_texture),
    Method(gl, texture_storage_2d),
    Method(gl, texture_sub_image_2d)
  );

  When(Method(gl, create_texture)).AlwaysReturn(name);

  const auto injector = make_injector(
    bind<iopengl>.to(gl.get())
  );

  auto texture = injector.create<le::gl::texture<tex2d>>();

  SECTION("creates texture")
  {
    Verify(Method(gl, create_texture).Using(value_of(texture.target)));
  }

  SECTION("destructor")
  {
    static_cast<le::gl::itexture&>(texture)
      .~itexture();

    SECTION("deletes texture")
    {
      Verify(Method(gl, delete_texture).Using(name));
    }
  }

  SECTION("upload")
  {
    const image image = {
      .width = 1,
      .height = 1,
      .pixels = {{0, 1, 2, 3}}
    };

    const auto& result = texture.upload(image);

    SECTION("returns self")
    {
      REQUIRE(&result == &texture);
    }

    SECTION("creates immutable storage if not already created")
    {
      Verify(
        Method(gl, texture_storage_2d).Using(
          texture.name(),
          texture.levels,
          value_of(texture.format),
          image.width,
          image.height
        )
      );
    }

    SECTION("does not create immutable storage if already created")
    {
      texture.upload(image);

      Verify(
        Method(gl, texture_storage_2d).Using(
          texture.name(),
          texture.levels,
          value_of(texture.format),
          image.width,
          image.height
        )
      ).Once();
    }

    SECTION("uploads pixel data")
    {
      Verify(
        Method(gl, texture_sub_image_2d).Using(
          texture.name(),
          0,
          0,
          0,
          image.width,
          image.height,
          base_of(texture.format),
          enum_for<gl_ubyte>(),
          static_cast<const void *>(image.pixels.data())
        )
      );
    }
  }

  SECTION("allocate_storage()")
  {
    // TODO
  }
}

TEST_CASE("texture<cubemap>", "[le][le::gl][le::gl::texture]")
{
  const gl_uint name = 123;

  Mock<iopengl> gl;

  Fake(
    Method(gl, delete_texture),
    Method(gl, texture_storage_2d),
    Method(gl, texture_sub_image_3d)
  );

  When(Method(gl, create_texture)).AlwaysReturn(name);

  const auto injector = make_injector(
    bind<iopengl>.to(gl.get())
  );

  auto texture = injector.create<le::gl::texture<le::gl::cubemap>>();

  SECTION("upload")
  {
    const gl_sizei width = 1;
    const gl_sizei height = 1;

    const std::array<std::vector<gl_ubyte>, 6> cubemap = {{
      {0, 1, 2, 3},
      {0, 1, 2, 3},
      {0, 1, 2, 3},
      {0, 1, 2, 3},
      {0, 1, 2, 3},
      {0, 1, 2, 3},
    }};

    const auto& result = texture.upload(width, height, cubemap);

    SECTION("returns self")
    {
      REQUIRE(&result == &texture);
    }

    SECTION("creates immutable storage if not already created")
    {
      Verify(
        Method(gl, texture_storage_2d).Using(
          texture.name(),
          texture.levels,
          value_of(texture.format),
          width,
          height
        )
      );
    }

    SECTION("does not create immutable storage if already created")
    {
      texture.upload(width, height, cubemap);

      Verify(
        Method(gl, texture_storage_2d).Using(
          texture.name(),
          texture.levels,
          value_of(texture.format),
          width,
          height
        )
      ).Once();
    }

    SECTION("uploads pixel data")
    {
      for (std::size_t i = 0; i < cubemap.size(); i++)
      {
        const auto& pixels = cubemap.at(i);

        Verify(
          Method(gl, texture_sub_image_3d).Using(
            texture.name(),
            0,
            0,
            0,
            i,
            width,
            height,
            1,
            base_of(texture.format),
            enum_for<gl_ubyte>(),
            static_cast<const void *>(pixels.data())
          )
        );
      }
    }
  }
}
