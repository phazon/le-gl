#include <catch2/catch.hpp>
#include <fakeit/fakeit.hpp>

#include <boost/di.hpp>
#include <glm/glm.hpp>

#include <le/gl/enum.hpp>
#include <le/gl/iopengl.hpp>
#include <le/gl/vertex_array.hpp>

using namespace boost::di;
using namespace fakeit;
using namespace glm;
using namespace le;
using namespace le::gl;

struct vertex
{
  vec3 position;
  vec4 colour;
};

TEST_CASE("vertex array", "[le][le::gl][le::gl::vertex_array]")
{
  const gl_uint name = 123;

  Mock<iopengl> gl;

  Fake(
    Method(gl, delete_vertex_array),
    Method(gl, vertex_array_attribute_format),
    Method(gl, enable_vertex_array_attribute),
    Method(gl, vertex_array_vertex_buffer),
    Method(gl, vertex_array_element_buffer)
  );

  When(Method(gl, create_vertex_array)).Return(name);

  const auto injector = make_injector(
    bind<iopengl>.to(gl.get())
  );

  auto vao = injector.create<vertex_array>();

  SECTION("creates vertex array")
  {
    Verify(Method(gl, create_vertex_array));
  }

  SECTION("returns name")
  {
    REQUIRE(vao.name() == name);
  }

  SECTION("destroys vertex array")
  {
    static_cast<ivertex_array&>(vao)
      .~ivertex_array();

    Verify(Method(gl, delete_vertex_array).Using(name));
  }

  SECTION("with_attribute_format()")
  {
    const gl_uint index = 1;
    const gl_int size = 3;
    const gl_uint relative_offset = 1;
    const bool normalized = true;

    const auto& result = vao.with_attribute_format<gl_float>(
      index,
      size,
      relative_offset,
      normalized
    );

    SECTION("returns self")
    {
      REQUIRE(&result == &vao);
    }

    SECTION("defines attribute format")
    {
      Verify(
        Method(gl, vertex_array_attribute_format).Using(
          vao.name(),
          index,
          size,
          enum_for<gl_float>(),
          normalized,
          relative_offset
        )
      );
    }
  }

  SECTION("with_vector_attribute<vec3>")
  {
    const gl_uint index = 1;
    const gl_int size = vec3::length();
    const gl_uint relative_offset = 1;
    const bool normalized = true;

    const auto& result = vao.with_vector_attribute<vec3>(
      index,
      relative_offset,
      normalized
    );

    SECTION("returns self")
    {
      REQUIRE(&result == &vao);
    }

    SECTION("defines attribute format")
    {
      Verify(
        Method(gl, vertex_array_attribute_format).Using(
          vao.name(),
          index,
          size,
          enum_for<vec3::value_type>(),
          normalized,
          relative_offset
        )
      );
    }
  }

  SECTION("with_vector_attribute<vec4>")
  {
    const gl_uint index = 1;
    const gl_int size = vec4::length();
    const gl_uint relative_offset = 1;
    const bool normalized = true;

    const auto& result = vao.with_vector_attribute<vec4>(
      index,
      relative_offset,
      normalized
    );

    SECTION("returns self")
    {
      REQUIRE(&result == &vao);
    }

    SECTION("defines attribute format")
    {
      Verify(
        Method(gl, vertex_array_attribute_format)
          .Using(
            vao.name(),
            index,
            size,
            enum_for<vec4::value_type>(),
            normalized,
            relative_offset
          )
      );
    }
  }

  SECTION("with_vector_attribute() of class member")
  {
    const gl_uint position_index = 1;
    const gl_int position_size = vec3::length();
    const gl_uint position_offset = offset_of(&vertex::position);
    const bool position_normalized = false;

    const gl_uint colour_index = 1;
    const gl_int colour_size = vec4::length();
    const gl_uint colour_offset = offset_of(&vertex::colour);
    const bool colour_normalized = false;

    const auto& position_result = vao.with_vector_attribute(
      position_index,
      &vertex::position
    );

    const auto& colour_result = vao.with_vector_attribute(
      colour_index,
      &vertex::colour
    );

    SECTION("returns self")
    {
      REQUIRE(&position_result == &vao);
      REQUIRE(&colour_result == &vao);
    }

    SECTION("defines attribute format")
    {
      Verify(
        Method(gl, vertex_array_attribute_format).Using(
          vao.name(),
          position_index,
          position_size,
          enum_for<vec3::value_type>(),
          position_normalized,
          position_offset
        )
      );

      Verify(
        Method(gl, vertex_array_attribute_format).Using(
          vao.name(),
          colour_index,
          colour_size,
          enum_for<vec4::value_type>(),
          colour_normalized,
          colour_offset
        )
      );
    }
  }

  SECTION("with_vector_attribute() of class member binding buffer")
  {
    const gl_uint buffer_name = 0xDEADBEEF;
    const gl_uint buffer_value_size = 21;

    Mock<ibuffer> buffer;
    When(Method(buffer, name)).AlwaysReturn(buffer_name);
    When(Method(buffer, value_size)).AlwaysReturn(buffer_value_size);

    const gl_uint position_index = 1;
    const gl_int position_size = vec3::length();
    const gl_uint position_offset = offset_of(&vertex::position);
    const bool position_normalized = true;

    const auto& result = vao.with_vector_attribute(
      position_index,
      &vertex::position,
      buffer.get(),
      position_normalized
    );

    SECTION("returns self")
    {
      REQUIRE(&result == &vao);
    }

    SECTION("defines attribute format")
    {
      Verify(
        Method(gl, vertex_array_attribute_format).Using(
          vao.name(),
          position_index,
          position_size,
          enum_for<vec3::value_type>(),
          position_normalized,
          position_offset
        )
      );
    }

    SECTION("binds vertex buffer to attribute")
    {
      Verify(
        Method(gl, vertex_array_vertex_buffer).Using(
          vao.name(),
          position_index,
          buffer_name,
          position_offset,
          buffer_value_size
        )
      );
    }
    
    SECTION("enables attribute")
    {
      Verify(
        Method(gl, enable_vertex_array_attribute)
          .Using(vao.name(), position_index)
      );
    }
  }

  SECTION("with_vector_attribute<vec3>() binding buffer")
  {
    const gl_uint buffer_name = 0xDEADBEEF;
    const gl_uint buffer_value_size = 21;

    Mock<ibuffer> buffer;
    When(Method(buffer, name)).AlwaysReturn(buffer_name);
    When(Method(buffer, value_size)).AlwaysReturn(buffer_value_size);

    const gl_uint position_index = 1;
    const gl_int position_size = vec3::length();
    const gl_uint position_offset = 2;
    const bool position_normalized = true;

    const auto& result = vao.with_vector_attribute<vec3>(
      position_index,
      buffer.get(),
      position_offset,
      position_normalized
    );

    SECTION("returns self")
    {
      REQUIRE(&result == &vao);
    }

    SECTION("defines attribute format")
    {
      Verify(
        Method(gl, vertex_array_attribute_format).Using(
          vao.name(),
          position_index,
          position_size,
          enum_for<vec3::value_type>(),
          position_normalized,
          position_offset
        )
      );
    }

    SECTION("binds vertex buffer to attribute")
    {
      Verify(
        Method(gl, vertex_array_vertex_buffer).Using(
          vao.name(),
          position_index,
          buffer_name,
          position_offset,
          buffer_value_size
        )
      );
    }
    
    SECTION("enables attribute")
    {
      Verify(
        Method(gl, enable_vertex_array_attribute).Using(
          vao.name(),
          position_index
        )
      );
    }
  }

  SECTION("enable_attribute()")
  {
    const gl_uint index = 1;

    const auto& result = vao.enable_attribute(index);

    SECTION("returns self")
    {
      REQUIRE(&result == &vao);
    }

    SECTION("enables attribute")
    {
      Verify(
        Method(gl, enable_vertex_array_attribute)
          .Using(vao.name(), index)
      );
    }
  }

  SECTION("vertex_buffer()")
  {
    const gl_uint index = 1;
    const gl_intptr offset = 1;
    const gl_uint buffer_name = 0xDEADBEEF;
    const gl_uint buffer_value_size = 21;

    Mock<ibuffer> buffer;
    When(Method(buffer, name)).AlwaysReturn(buffer_name);
    When(Method(buffer, value_size)).AlwaysReturn(buffer_value_size);

    const auto& result = vao.vertex_buffer(index, buffer.get(), offset);

    SECTION("returns self")
    {
      REQUIRE(&result == &vao);
    }

    SECTION("sets vertex buffer")
    {
      Verify(Method(buffer, name));
      Verify(Method(buffer, value_size));

      Verify(
        Method(gl, vertex_array_vertex_buffer).Using(
          vao.name(),
          index,
          buffer_name,
          offset,
          buffer_value_size
        )
      );
    }
  }

  SECTION("element_buffer()")
  {
    const gl_uint buffer_name = 0xDEADBEEF;

    Mock<ibuffer> buffer;
    When(Method(buffer, name)).AlwaysReturn(buffer_name);

    const auto& result = vao.element_buffer(buffer.get());

    SECTION("returns self")
    {
      REQUIRE(&result == &vao);
    }

    SECTION("sets element buffer")
    {
      Verify(
        Method(gl, vertex_array_element_buffer).Using(
          vao.name(),
          buffer_name
        )
      );
    }
  }
}