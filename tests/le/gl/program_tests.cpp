#include <string>

#include <catch2/catch.hpp>
#include <fakeit/fakeit.hpp>

#include <boost/di.hpp>

#include <le/gl/iopengl.hpp>
#include <le/gl/ishader.hpp>
#include <le/gl/program.hpp>

using namespace std::string_literals;
using namespace boost::di;
using namespace fakeit;
using namespace le;
using namespace le::gl;

TEST_CASE("program", "[le][le::gl][le::gl::program]")
{
  const gl_uint name = 123;

  Mock<iopengl> gl;

  Fake(
    Method(gl, delete_program),
    Method(gl, attach_shader),
    Method(gl, link_program),
    Method(gl, program_uniform_matrix4fv)
  );

  When(Method(gl, create_program)).Return(name);

  const auto injector = make_injector(
    bind<iopengl>.to(gl.get())
  );

  auto program = injector.create<le::gl::program>();

  SECTION("creates program")
  {
    Verify(Method(gl, create_program));
  }

  SECTION("returns name")
  {
    REQUIRE(program.name() == name);
  }

  SECTION("destroys program")
  {
    static_cast<le::gl::iprogram&>(program)
      .~iprogram();

    Verify(Method(gl, delete_program).Using(name));
  }

  SECTION("attach_shader()")
  {
    const gl_uint shader_name = 345;

    Mock<ishader> shader;

    When(Method(shader, name)).Return(shader_name);

    const auto& result = program.attach_shader(shader.get());

    SECTION("attaches shader")
    {
      Verify(
        Method(shader, name),
        Method(gl, attach_shader).Using(name, shader_name)
      );
    }

    SECTION("returns program")
    {
      REQUIRE(&result == &program);
    }
  }

  SECTION("link()")
  {
    const auto& result = program.link();

    SECTION("links program")
    {
      Verify(Method(gl, link_program).Using(name));
    }

    SECTION("returns program")
    {
      REQUIRE(&result == &program);
    }
  }

  SECTION("returns location of attribute")
  {
    const auto attribute = "hello"s;
    const gl_int expected_location = 21;

    When(Method(gl, get_attribute_location).Using(name, attribute))
      .Return(expected_location);

    REQUIRE(program.attribute_location(attribute) == expected_location);
  }

  SECTION("returns location of uniform")
  {
    const auto attribute = "hello"s;
    const gl_int expected_location = 20;

    When(Method(gl, get_uniform_location).Using(name, attribute))
      .Return(expected_location);

    REQUIRE(program.uniform_location(attribute) == expected_location);
  }

  SECTION("sets mat4 uniform")
  {
    const auto value = mat4();

    SECTION("for given location")
    {
      const gl_int location = 21;

      program.uniform(location, value);

      Verify(
        Method(gl, program_uniform_matrix4fv).Using(
          name,
          location,
          false,
          value_ptr(value)
        )
      );
    }

    SECTION("for name")
    {
      const gl_int location = 21;
      const auto uniform = "hello"s;

      When(Method(gl, get_uniform_location).Using(name, uniform))
        .Return(location);

      program.uniform(uniform, value);

      Verify(
        Method(gl, program_uniform_matrix4fv).Using(
          name,
          location,
          false,
          value_ptr(value)
        )
      );
    }
  }
}