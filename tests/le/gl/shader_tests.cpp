#include <catch2/catch.hpp>
#include <fakeit/fakeit.hpp>

#include <boost/di.hpp>

#include <le/gl/iopengl.hpp>
#include <le/gl/shader.hpp>

using namespace std::string_literals;
using namespace boost::di;
using namespace fakeit;
using namespace le;
using namespace le::gl;

TEST_CASE("shader", "[le][le::gl][le::gl::shader]")
{
  const gl_uint name = 123;

  Mock<iopengl> gl;

  Fake(
    Method(gl, shader_source),
    Method(gl, compile_shader),
    Method(gl, delete_shader)
  );

  When(Method(gl, create_shader)).Return(name);

  const auto injector = make_injector(
    bind<iopengl>.to(gl.get())
  );

  auto shader = injector.create<le::gl::shader<vertex>>();

  SECTION("creates shader of given type")
  {
    Verify(
      Method(gl, create_shader).Using(value_of(le::gl::vertex))
    );
  }

  SECTION("returns name")
  {
    REQUIRE(shader.name() == name);
  }

  SECTION("set_source()")
  {
    const auto source = "sauce"s;

    auto& result = shader.set_source(source);

    SECTION("sets shader source")
    {
      Verify(Method(gl, shader_source).Using(name, source));
    }

    SECTION("returns shader")
    {
      REQUIRE(&result == &shader);
    }
  }

  SECTION("compile()")
  {
    auto& result = shader.compile();

    SECTION("compiles shader")
    {
      Verify(Method(gl, compile_shader).Using(name));
    }

    SECTION("returns shader")
    {
      REQUIRE(&result == &shader);
    }
  }

  SECTION("destructor")
  {
    static_cast<le::gl::ishader&>(shader)
      .~ishader();

    SECTION("deletes shader")
    {
      Verify(Method(gl, delete_shader).Using(name));
    }
  }
}