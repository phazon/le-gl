#include <catch2/catch.hpp>
#include <fakeit/fakeit.hpp>

#include <boost/di.hpp>

#include <le/gl/iopengl.hpp>
#include <le/gl/buffer.hpp>
#include <le/gl/enum.hpp>

using namespace boost::di;
using namespace fakeit;
using namespace le;
using namespace le::gl;

TEST_CASE("buffer", "[le][le::gl][le::gl::buffer]")
{
  const gl_uint name = 123;

  Mock<iopengl> gl;

  Fake(
    Method(gl, delete_buffer),
    Method(gl, named_buffer_data)
  );

  When(Method(gl, create_buffer)).AlwaysReturn(name);

  const auto injector = make_injector(
    bind<iopengl>.to(gl.get())
  );

  auto buffer = injector.create<le::gl::buffer<gl_float>>();

  SECTION("creates buffer")
  {
    Verify(Method(gl, create_buffer));
  }

  SECTION("returns name")
  {
    REQUIRE(buffer.name() == name);
  }

  SECTION("destructor")
  {
    static_cast<ibuffer&>(buffer)
      .~ibuffer();

    SECTION("deletes buffer")
    {
      Verify(Method(gl, delete_buffer).Using(name));
    }
  }

  SECTION("set_data()")
  {
    const auto usage = static_draw;

    SECTION("from raw pointer")
    {
      const gl_size size = 21;
      const auto data = 32;

      auto& result = buffer.set_data(size, &data, usage);

      SECTION("sets data")
      {
        Verify(
          Method(gl, named_buffer_data).Using(name, size, &data, value_of(usage))
        );
      }

      SECTION("returns buffer")
      {
        REQUIRE(&result == &buffer);
      }

      SECTION("has length")
      {
        REQUIRE(buffer.length() == size / buffer.value_size());
      }
    }

    SECTION("from vector")
    {
      const std::vector<gl_float> data = {1, 2, 3};

      auto& result = buffer.set_data(data, usage);

      SECTION("sets data")
      {
        Verify(
          Method(gl, named_buffer_data)
            .Using(
              name,
              size(data),
              data.data(),
              value_of(usage)
            )
        );
      }

      SECTION("returns buffer")
      {
        REQUIRE(&result == &buffer);
      }

      SECTION("has length")
      {
        REQUIRE(buffer.length() == size(data) / buffer.value_size());
      }
    }
  }

  SECTION("value_size() returns size of underlying data type")
  {
    REQUIRE(buffer.value_size() == sizeof(decltype(buffer)::value_type));
  }

  SECTION("type()")
  {
    #define ENUM_FOR_TYPE(gl_type, enum) \
      SECTION("returns enum " #enum " for type " #gl_type) \
      { \
        const auto buffer = injector.create<le::gl::buffer<gl_type>>(); \
        REQUIRE(buffer.type() == enum); \
      }

    #include <le/gl/enum_for_type.def>
  }
}
