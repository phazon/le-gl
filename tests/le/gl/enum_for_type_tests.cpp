#include <catch2/catch.hpp>

#include <le/gl/enum.hpp>

using namespace le::gl;

TEST_CASE("enum for type", "[le][le::gl][le::gl::enum]")
{
  #define ENUM_FOR_TYPE(type, enum) \
    SECTION("returns enum: " #enum " for type: " #type) \
    { \
      REQUIRE(enum_for<type>() == enum); \
    }

  #include <le/gl/enum_for_type.def>
}