#include <shared/image.hpp>

#define STB_IMAGE_IMPLEMENTATION
#include <stb/stb_image.h>

namespace le::gl
{
  const image load_image(const std::string& filename)
  {
    ivec2 dimensions;
    int components;

    auto data = stbi_load(
      filename.c_str(),
      &dimensions.x,
      &dimensions.y,
      &components,
      STBI_rgb_alpha
    );

    assert(data != nullptr);

    const auto bytes = (dimensions.x * dimensions.y) * 4;

    return {
      .dimensions = dimensions,
      .pixels = std::vector<gl_ubyte>(data, data + bytes)
    };
  }

  const image load_image(void)
  {
    return load_image("albedo.png");
  }

  const std::array<image, 6> load_cubemap(void)
  {
    return {
      load_image("right.jpg"),
      load_image("left.jpg"),
      load_image("top.jpg"),
      load_image("bottom.jpg"),
      load_image("front.jpg"),
      load_image("back.jpg")
    };
  }
}
