#pragma once

#include <le/glfw/window.hpp>
#include <le/gl/iopengl.hpp>
#include <shared/overloaded.hpp>

class framebuffer_size_handler: public le::glfw::window::event_handler
{
public:

  framebuffer_size_handler(le::gl::iopengl& gl) : gl(gl) { }

  std::set<event_type> handled(void) const override
  {
    return {framebuffer_size};
  }

  void handle(le::glfw::iwindow& window, const event& event) override
  {
    std::visit(
      overloaded
      {
        [this](const framebuffer_size_event& framebuffer)
        {
          update_viewport(framebuffer);
        }
      },
      event
    );
  }

private:

  le::gl::iopengl& gl;

  void update_viewport(const framebuffer_size_event& framebuffer) const
  {
    gl.viewport(0, 0, framebuffer.width, framebuffer.height);
  }
};