#pragma once

#include <algorithm>
#include <functional>
#include <type_traits>

#include <le/gl/types.hpp>
#include <le/gl/texture.hpp>

template<typename T, size_t N, typename F>
auto map(const std::array<T, N>& array, F f)
{
  std::array<std::invoke_result_t<F, T>, N> result;

  std::transform(array.begin(), array.end(), result.begin(), f);

  return result;
}

namespace le::gl
{
  struct image
  {
    const ivec2 dimensions;
    const std::vector<gl_ubyte> pixels;
  };

  const image load_image(void);

  const std::array<image, 6> load_cubemap(void);

  template<texture_target T, texture_format F, int L>
  void upload(texture<T, F, L>& texture, const image& image)
  {
    texture.upload(image.dimensions.x, image.dimensions.y, image.pixels);
  }

  template<texture_target T, texture_format F, int L>
  void upload(texture<T, F, L>& texture, const std::array<image, 6>& images)
  {
    const auto cubemap =
      map(images, [](const auto& image) {
        return image.pixels;
      });

    const auto dimensions = images[0].dimensions;

    texture.upload(dimensions.x, dimensions.y, cubemap);
  }
}
