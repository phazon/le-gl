#include <le/glfw/module.hpp>
#include <le/gl/module.hpp>

#include <le/glfw/gl.hpp>
#include <le/gl/enum.hpp>
#include <le/gl/bitfield.hpp>
#include <le/gl/shader.hpp>
#include <le/gl/program.hpp>
#include <le/gl/vertex_array.hpp>
#include <le/gl/buffer.hpp>
#include <le/gl/texture.hpp>
#include <le/gl/maths.hpp>

#include <shared/image.hpp>
#include <shared/framebuffer_size_handler.hpp>

using namespace boost;

using namespace le;
using namespace le::gl;
using namespace le::glfw;

const auto injector = di::make_injector(
  le::glfw::module(),
  le::gl::module(),
  di::bind<window::event_handler*[]>.to<framebuffer_size_handler>()
);

const char *model_vertex_shader_source = R"""(
  #version 450 core

  in vec3 position;
  in vec2 tex_coord;

  out vec2 f_tex_coord;

  uniform mat4 mvp;

  void main()
  {
    f_tex_coord = tex_coord;
    gl_Position = mvp * vec4(position.xyz, 1.0);
  }
)""";

const char *model_fragment_shader_source = R"""(
  #version 450 core

  in vec2 f_tex_coord;

  uniform sampler2D albedo;

  out vec4 colour;

  void main()
  {
    colour = texture(albedo, f_tex_coord);
  }
)""";

const char *cubemap_vertex_shader_source = R"""(
  #version 450 core

  in vec3 position;

  out vec3 f_position;

  uniform mat4 mvp;

  void main()
  {
    f_position = position;
    gl_Position = (mvp * vec4(position.xyz, 1.0)).xyww;
  }
)""";

const char *cubemap_fragment_shader_source = R"""(
  #version 450 core

  in vec3 f_position;

  uniform samplerCube cubemap;

  out vec4 colour;

  void main()
  {
    colour = texture(cubemap, f_position);
  }
)""";

const window_settings settings = {
  .samples = 16,
  .context_version_major = 4,
  .context_version_minor = 5,
  .opengl_profile = GLFW_OPENGL_CORE_PROFILE,
};

struct vertex_t
{
  vec3 position;
  vec2 tex_coord;
};

const std::vector<vertex_t> cube = {
  {{-0.5f, 0.5f, 0.5f}, {0.f, 0.f}},
  {{-0.5f,-0.5f, 0.5f}, {0.f, 1.f}},
  {{ 0.5f, 0.5f, 0.5f}, {1.f, 0.f}},
  {{ 0.5f,-0.5f, 0.5f}, {1.f, 1.f}},

  {{-0.5f, 0.5f,-0.5f}, {0.f, 0.f}},
  {{-0.5f,-0.5f,-0.5f}, {0.f, 1.f}},
  {{ 0.5f, 0.5f,-0.5f}, {1.f, 0.f}},
  {{ 0.5f,-0.5f,-0.5f}, {1.f, 1.f}},

  {{-0.5f, 0.5f, 0.5f}, {0.f, 0.f}},
  {{-0.5f,-0.5f, 0.5f}, {0.f, 1.f}},
  {{-0.5f, 0.5f,-0.5f}, {1.f, 0.f}},
  {{-0.5f,-0.5f,-0.5f}, {1.f, 1.f}},

  {{ 0.5f, 0.5f, 0.5f}, {0.f, 0.f}},
  {{ 0.5f,-0.5f, 0.5f}, {0.f, 1.f}},
  {{ 0.5f, 0.5f,-0.5f}, {1.f, 0.f}},
  {{ 0.5f,-0.5f,-0.5f}, {1.f, 1.f}},

  {{-0.5f, 0.5f, 0.5f}, {0.f, 0.f}},
  {{ 0.5f, 0.5f, 0.5f}, {0.f, 1.f}},
  {{-0.5f, 0.5f,-0.5f}, {1.f, 0.f}},
  {{ 0.5f, 0.5f,-0.5f}, {1.f, 1.f}},

  {{-0.5f,-0.5f, 0.5f}, {0.f, 0.f}},
  {{ 0.5f,-0.5f, 0.5f}, {0.f, 1.f}},
  {{-0.5f,-0.5f,-0.5f}, {1.f, 0.f}},
  {{ 0.5f,-0.5f,-0.5f}, {1.f, 1.f}},
};

const std::vector<vec3> cubemap = {
  {-1.f, 1.f, 1.f},
  {-1.f,-1.f, 1.f},
  { 1.f, 1.f, 1.f},
  { 1.f,-1.f, 1.f},

  {-1.f, 1.f,-1.f},
  {-1.f,-1.f,-1.f},
  { 1.f, 1.f,-1.f},
  { 1.f,-1.f,-1.f},

  {-1.f, 1.f, 1.f},
  {-1.f,-1.f, 1.f},
  {-1.f, 1.f,-1.f},
  {-1.f,-1.f,-1.f},

  { 1.f, 1.f, 1.f},
  { 1.f,-1.f, 1.f},
  { 1.f, 1.f,-1.f},
  { 1.f,-1.f,-1.f},

  {-1.f, 1.f, 1.f},
  { 1.f, 1.f, 1.f},
  {-1.f, 1.f,-1.f},
  { 1.f, 1.f,-1.f},

  {-1.f,-1.f, 1.f},
  { 1.f,-1.f, 1.f},
  {-1.f,-1.f,-1.f},
  { 1.f,-1.f,-1.f},
};

const std::vector<gl_uint> indices = {
  0, 1, 2,
  2, 1, 3,

  4, 6, 5,
  5, 6, 7,

  8, 10, 9,
  9, 10, 11,

  12, 13, 14,
  14, 13, 15,

  16, 17, 18,
  18, 17, 19,

  20, 21, 22,
  22, 21, 23
};

const auto view = look_at(
  vec3{0.f, 2.f, 5.f}, // position
  vec3{0.f, 0.f, 0.f}, // target
  vec3{0.f, 1.f, 0.f}  // up axis
);

auto cube_model = mat4(1.f);

int main(void)
{
  auto& glfw = injector.create<iglfw&>();
  auto& window = injector.create<iwindow&>();
  auto& gl = injector.create<iopengl&>();

  window.create(640, 480, "Cube", settings);
  window.make_context_current();

  gl.initialize(get_proc_address);

  auto model_program = injector.create<program>()
    .attach_shader(injector
      .create<shader<vertex>>()
      .set_source(model_vertex_shader_source)
      .compile()
    )
    .attach_shader(injector
      .create<shader<fragment>>()
      .set_source(model_fragment_shader_source)
      .compile()
    )
    .link();

  auto model_vbo = injector.create<buffer<vertex_t>>()
    .set_data(cube, static_draw);

  auto model_ebo = injector.create<buffer<gl_uint>>()
    .set_data(indices, static_draw);

  auto model_vao = injector.create<vertex_array>()
    .with_vector_attribute(
      model_program.attribute_location("position"),
      &vertex_t::position,
      model_vbo
    )
    .with_vector_attribute(
      model_program.attribute_location("tex_coord"),
      &vertex_t::tex_coord,
      model_vbo
    )
    .element_buffer(model_ebo);

  auto model_texture = injector.create<texture2d<>>()
    .upload(load_image());

  auto cubemap_program = injector.create<program>()
    .attach_shader(injector
      .create<shader<vertex>>()
      .set_source(cubemap_vertex_shader_source)
      .compile()
    )
    .attach_shader(injector
      .create<shader<fragment>>()
      .set_source(cubemap_fragment_shader_source)
      .compile()
    )
    .link();

  auto cubemap_vbo = injector.create<buffer<vec3>>()
    .set_data(::cubemap, static_draw);

  auto cubemap_ebo = injector.create<buffer<gl_uint>>()
    .set_data(indices, static_draw);

  auto cubemap_vao = injector.create<vertex_array>()
    .with_vector_attribute<vec3>(
      cubemap_program.attribute_location("position"),
      cubemap_vbo
    )
    .element_buffer(cubemap_ebo);

  auto cubemap_texture = injector.create<textureCube<>>()
    .upload(load_cubemap());

  gl.enable(GL_DEPTH_TEST);
  gl.clear_color({0.f, 0.f, 0.f, 0.f});  

  while (window.is_open())
  {
    const auto [width, height] =
      static_cast<std::tuple<float, float>>(
        window.framebuffer_size()
      );

    const auto projection = perspective(
      radians(45.f),  // y fov
      width / height, // aspect ratio
      0.1f,           // near clip plane
      10.f            // far clip plane
    );

    glfw.poll_events();

    if (window.key(le::glfw::key::Escape))
    {
      window.close();
    }

    gl.clear(GL_COLOR_BUFFER_BIT | GL_DEPTH_BUFFER_BIT);

    cube_model = rotate(
      cube_model,
      0.001f * radians(180.0f),
      vec3(0.0f, 1.0f, 0.0f)
    );

    model_program.uniform("mvp", projection * view * cube_model);

    gl.use_program(model_program);
    gl.bind_vertex_array(model_vao);
    gl.bind_texture_unit(0, model_texture);
    gl.depth_func(GL_LESS);
    model_vao.draw_elements();

    cubemap_program.uniform("mvp", projection * mat4(mat3(view)) * cube_model);

    gl.use_program(cubemap_program);
    gl.bind_vertex_array(cubemap_vao);
    gl.bind_texture_unit(0, cubemap_texture);
    gl.enable(GL_TEXTURE_CUBE_MAP_SEAMLESS);
    gl.depth_func(GL_LEQUAL);
    cubemap_vao.draw_elements();
    gl.disable(GL_TEXTURE_CUBE_MAP_SEAMLESS);

    window.swap_buffers();
  }

  return 0;
}