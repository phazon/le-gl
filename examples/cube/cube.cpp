#include <le/glfw/module.hpp>
#include <le/gl/module.hpp>

#include <le/glfw/gl.hpp>
#include <le/gl/enum.hpp>
#include <le/gl/bitfield.hpp>
#include <le/gl/shader.hpp>
#include <le/gl/program.hpp>
#include <le/gl/vertex_array.hpp>
#include <le/gl/buffer.hpp>
#include <le/gl/maths.hpp>

using namespace boost;

using namespace le;
using namespace le::gl;
using namespace le::glfw;

const auto injector = di::make_injector(
  le::glfw::module(),
  le::gl::module()
);

const char *vertex_shader_source = R"""(
  #version 450 core

  in vec3 position;
  in vec4 colour;

  out vec4 f_colour;

  uniform mat4 mvp;

  void main()
  {
    f_colour = colour;
    gl_Position = mvp * vec4(position.xyz, 1.0);
  }
)""";

const char *fragment_shader_source = R"""(
  #version 450 core

  in vec4 f_colour;

  out vec4 colour;

  void main()
  {
    colour = f_colour;
  }
)""";

const window_settings settings = {
  .samples = 16,
  .context_version_major = 4,
  .context_version_minor = 5,
  .opengl_profile = GLFW_OPENGL_CORE_PROFILE,
};

struct vertex_t
{
  vec3 position;
  vec4 colour;
};

const std::vector<vertex_t> vertices = {
  {{-0.5f, 0.5f, 0.5f}, {0.0f, 1.0f, 0.0f, 1.0f}},
  {{-0.5f,-0.5f, 0.5f}, {0.0f, 1.0f, 0.0f, 1.0f}},
  {{ 0.5f, 0.5f, 0.5f}, {0.0f, 0.0f, 1.0f, 1.0f}},
  {{ 0.5f,-0.5f, 0.5f}, {0.0f, 0.0f, 1.0f, 1.0f}},

  {{-0.5f, 0.5f,-0.5f}, {0.0f, 0.0f, 1.0f, 1.0f}},
  {{-0.5f,-0.5f,-0.5f}, {0.0f, 0.0f, 1.0f, 1.0f}},
  {{ 0.5f, 0.5f,-0.5f}, {1.0f, 0.0f, 0.0f, 1.0f}},
  {{ 0.5f,-0.5f,-0.5f}, {1.0f, 0.0f, 0.0f, 1.0f}},

  {{-0.5f, 0.5f, 0.5f}, {0.0f, 1.0f, 0.0f, 1.0f}},
  {{-0.5f,-0.5f, 0.5f}, {0.0f, 1.0f, 0.0f, 1.0f}},
  {{-0.5f, 0.5f,-0.5f}, {0.0f, 0.0f, 1.0f, 1.0f}},
  {{-0.5f,-0.5f,-0.5f}, {0.0f, 0.0f, 1.0f, 1.0f}},

  {{ 0.5f, 0.5f, 0.5f}, {0.0f, 0.0f, 1.0f, 1.0f}},
  {{ 0.5f,-0.5f, 0.5f}, {0.0f, 0.0f, 1.0f, 1.0f}},
  {{ 0.5f, 0.5f,-0.5f}, {1.0f, 0.0f, 0.0f, 1.0f}},
  {{ 0.5f,-0.5f,-0.5f}, {1.0f, 0.0f, 0.0f, 1.0f}},

  {{-0.5f, 0.5f, 0.5f}, {0.0f, 1.0f, 0.0f, 1.0f}},
  {{ 0.5f, 0.5f, 0.5f}, {0.0f, 0.0f, 1.0f, 1.0f}},
  {{-0.5f, 0.5f,-0.5f}, {0.0f, 0.0f, 1.0f, 1.0f}},
  {{ 0.5f, 0.5f,-0.5f}, {1.0f, 0.0f, 0.0f, 1.0f}},

  {{-0.5f,-0.5f, 0.5f}, {0.0f, 1.0f, 0.0f, 1.0f}},
  {{ 0.5f,-0.5f, 0.5f}, {0.0f, 0.0f, 1.0f, 1.0f}},
  {{-0.5f,-0.5f,-0.5f}, {0.0f, 0.0f, 1.0f, 1.0f}},
  {{ 0.5f,-0.5f,-0.5f}, {1.0f, 0.0f, 0.0f, 1.0f}},
};

const std::vector<gl_uint> indices = {
  0, 1, 2,
  2, 1, 3,

  4, 6, 5,
  5, 6, 7,

  8, 10, 9,
  9, 10, 11,

  12, 13, 14,
  14, 13, 15,

  16, 17, 18,
  18, 17, 19,

  20, 21, 22,
  22, 21, 23
};

const auto projection = perspective(
  radians(45.f), // y fov
  640.f / 480.f, // aspect ratio
  0.1f,          // near clip plane
  10.f           // far clip plane
);

const auto view = look_at(
  vec3{0.f, 2.f, 5.f}, // position
  vec3{0.f, 0.f, 0.f}, // target
  vec3{0.f, 1.f, 0.f}  // up axis
);

mat4 model(1.f);

int main(void)
{
  auto& glfw = injector.create<iglfw&>();
  auto& window = injector.create<iwindow&>();
  auto& gl = injector.create<iopengl&>();

  window.create(640, 480, "Cube", settings);
  window.make_context_current();

  gl.initialize(get_proc_address);

  auto pipeline = injector.create<program>()
    .attach_shader(injector
      .create<shader<vertex>>()
      .set_source(vertex_shader_source)
      .compile()
    )
    .attach_shader(injector
      .create<shader<fragment>>()
      .set_source(fragment_shader_source)
      .compile()
    )
    .link();

  const auto position = pipeline.attribute_location("position");
  const auto colour = pipeline.attribute_location("colour");

  auto vbo = injector.create<buffer<vertex_t>>()
    .set_data(vertices, static_draw);

  auto ebo = injector.create<buffer<gl_uint>>()
    .set_data(indices, static_draw);

  auto vao = injector.create<vertex_array>()
    .with_vector_attribute(position, &vertex_t::position, vbo)
    .with_vector_attribute(colour, &vertex_t::colour, vbo)
    .element_buffer(ebo);

  gl.enable(GL_DEPTH_TEST);
  gl.clear_color({0.f, 0.f, 0.f, 0.f});
  gl.use_program(pipeline);
  gl.bind_vertex_array(vao);

  while (window.is_open())
  {
    glfw.poll_events();

    if (window.key(le::glfw::key::Escape))
    {
      window.close();
    }

    gl.clear(GL_COLOR_BUFFER_BIT | GL_DEPTH_BUFFER_BIT);
    
    model = rotate(
      model,
      0.001f * radians(180.0f),
      glm::vec3(0.0f, 1.0f, 0.0f)
    );

    pipeline.uniform("mvp", projection * view * model);

    vao.draw_elements();

    window.swap_buffers();
  }

  return 0;
}