#include <le/glfw/module.hpp>
#include <le/gl/module.hpp>

#include <le/glfw/gl.hpp>
#include <le/gl/bitfield.hpp>

using namespace boost;
using namespace le;
using namespace le::gl;
using namespace le::glfw;

const auto injector = di::make_injector(
  le::glfw::module(),
  le::gl::module()
);

const window_settings settings = {
  .context_version_major = 4,
  .context_version_minor = 5,
  .opengl_profile = GLFW_OPENGL_CORE_PROFILE
};

int main(void)
{
  auto& glfw = injector.create<iglfw&>();
  auto& window = injector.create<iwindow&>();
  auto& gl = injector.create<iopengl&>();

  window.create(640, 480, "hello", settings);
  window.make_context_current();

  gl.initialize(get_proc_address);
  gl.clear_color({1.f, 1.f, 1.f, 1.f});

  while (window.is_open())
  {    
    glfw.poll_events();

    if (window.key(key::Escape))
    {
      window.close();
    }

    gl.clear(GL_COLOR_BUFFER_BIT);
    
    window.swap_buffers();
  }

  return 0;
}